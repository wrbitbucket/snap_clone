//
//  Alerta.swift
//  SnapClone
//
//  Created by Wagner Rodrigues on 01/10/17.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

import UIKit

class Alerta{

    var titulo: String
    var mensagem: String
    
    init(titulo: String, mensagem: String){
        self.titulo = titulo
        self.mensagem = mensagem
    }
    
    func getAlerta() -> UIAlertController{
    
        let alerta = UIAlertController.init(title: titulo, message: mensagem, preferredStyle: .alert)
        let acaoCancelar = UIAlertAction.init(title: "Cancelar", style: .cancel, handler: nil)
        
        alerta.addAction(acaoCancelar)
        return alerta
    }
}
