//
//  CadastrarViewController.swift
//  SnapClone
//
//  Created by Wagner Rodrigues on 23/09/17.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class CadastrarViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var nomeCompleto: UITextField!
    @IBOutlet weak var senha: UITextField!
    @IBOutlet weak var confirmarSenha: UITextField!
    
    
    func exibirMensagem(titulo:String, mensagem: String){
        
        let alerta = UIAlertController.init(title: titulo, message: mensagem, preferredStyle: .alert)
        let acaoCancelar = UIAlertAction.init(title: "Cancelar", style: .cancel, handler: nil)
        
        alerta.addAction(acaoCancelar)
        present(alerta, animated: true, completion: nil)
    }
    
    @IBAction func cirarConta(_ sender: Any) {
        //RECUPERARA DADOS DIGITADOS
        if let emailR = self.email.text{
            if let nomeCompletoR = self.nomeCompleto.text{
                if let senhaR = self.senha.text{
                    if let senhaConfirmacaoR = self.confirmarSenha.text{
                        
                        //VALIDAR SENHA
                        if senhaR == senhaConfirmacaoR {
                            if nomeCompletoR != ""{
                                
                                //CRIAR CONTA FIREBASE
                                let autenticacao = Auth.auth()
                                autenticacao.createUser(withEmail: emailR, password: senhaR , completion: { (usuario, erro) in
                                    if erro == nil{
                                        if usuario == nil{
                                            let alerta = Alerta(titulo: "Erro ao autenticar", mensagem: "Problema na autenticação, tente novamente.")
                                            self.present(alerta.getAlerta(), animated: true, completion: nil)
                                        }else{
                                            //REDIRECIONA USUARIO PARA TELA PRINCIPAL
                                            
                                            let dataBase = Database.database().reference()
                                            let usuarios = dataBase.child("usuarios")
                                            
                                            let usuarioDados = ["nome": nomeCompletoR, "email":emailR]
                                            
                                            usuarios.child(usuario!.uid).setValue(usuarioDados)
                                            
                                            self.performSegue(withIdentifier: "cadastroLoginSegue", sender: nil)
                                        }
                                    }else{
                                        let erroR = erro! as NSError
                                        
                                        if let codErro = erroR.userInfo["error_name"]{
                                            
                                            let erroTexto = codErro as! String
                                            var msnErro = ""
                                            switch erroTexto{
                                            case "ERROR_INVALID_EMAIL" : msnErro = "Email inválido, digite um email válido!"
                                                break
                                            case "ERROR_WEAK_PASSWORD" : msnErro = "Senha precisa ter no mínimo 6 caracteres, com letras e numeros"
                                                break
                                            case "ERROR_EMAIL_ALREADY_IN_USE" : msnErro = "Email ja esta sendo utilizado, crie sua conta com outro email"
                                                break
                                            default: msnErro = "Dados digitados estao incorretos."
                                            }
                                            let alerta = Alerta(titulo: "Dados invalidos", mensagem: "Dados digitados incorretos")
                                            self.present(alerta.getAlerta(), animated: true, completion: nil)
                                        }
                                    }
                                })
                            }else{
                                let alerta = Alerta(titulo: "Dados incorretos", mensagem: "Digite seu nome para prosseguir!")
                                self.present(alerta.getAlerta(), animated: true, completion: nil)
                            }
                        }else{
                            self.exibirMensagem(titulo: "Dados incorretos", mensagem: "As senhas precisam ser iguais, digite novamente.")
                        }//FIM VALIDAR SENHA
                        
                    }
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        email.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
