//
//  EntrarViewController.swift
//  SnapClone
//
//  Created by Wagner Rodrigues on 23/09/17.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

import UIKit
import FirebaseAuth

class EntrarViewController: UIViewController {
    
    
    @IBOutlet weak var email: UITextField!

    @IBOutlet weak var senha: UITextField!
    
    
    @IBAction func entrar(_ sender: Any) {
        
        //RECUOERAR DADOS DIGITADOS
        if let emailR = self.email.text{
            if let senhaR = self.senha.text{
                //AUTENTICAR USUARIO NO FIREBASE
                let autenticacao = Auth.auth()
                autenticacao.signIn(withEmail: emailR, password: senhaR, completion: { (usuario, erro) in
                    if erro == nil{
                        if usuario == nil{
                            self.exibirMensagem(titulo: "Erro ao autenticar ", mensagem: "Problema ao realizar autenticação, tente novamente.")
                        }else{
                            //REDIRECIONA USUARIO PARA TELA PRINCIPAL
                            self.performSegue(withIdentifier: "loginSegue", sender: nil)
                        }
                    }else{
                        self.exibirMensagem(titulo: "Dados incorretos.", mensagem: "Verifique os dados digitados e tente novamente.")
                    }
                })
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func exibirMensagem(titulo:String, mensagem: String){
        
        let alerta = UIAlertController.init(title: titulo, message: mensagem, preferredStyle: .alert)
        let acaoCancelar = UIAlertAction.init(title: "Cancelar", style: .cancel, handler: nil)
        
        alerta.addAction(acaoCancelar)
        present(alerta, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
