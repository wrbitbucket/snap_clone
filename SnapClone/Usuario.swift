//
//  Usuario.swift
//  SnapClone
//
//  Created by Wagner Rodrigues on 01/10/17.
//  Copyright © 2017 Wagner Rodrigues. All rights reserved.
//

import Foundation

class Usuario{
    var email: String
    var nome: String
    var uid: String
    
    init(email: String, nome: String, uid:String){
        self.email = email
        self.nome = nome
        self.uid = uid
    }
}
